const express = require('express')
const app = express()
const cors = require('cors')
const mongoose = require('mongoose')
const dotenv = require('dotenv')
const connectDB = require("./config/db");


dotenv.config({ path: './.env' });
connectDB();


// const DB = process.env.DATABASE.replace('<PASSWORD>', process.env.DATABASE_PASSWORD)

const authRouter = require('./routes/auth')
const questionRouter = require('./routes/questionRoutes')
const certificateRouter = require('./routes/communityRoute')
const marriageCertificateRouter = require("./routes/wedLock")
app.use(cors())

app.use(express.json())
app.use('/api/auth', authRouter)
app.use('/api/question', questionRouter)
app.use('/api/community', certificateRouter)
app.use('/api/wedLock', marriageCertificateRouter)

// mongoose.connect("mongodb+srv://12220057:vlU47A9CRzOcw5sm@cluster0.bhmsq.mongodb.net/?retryWrites=true&w=majority&appName=Cluster0"
// ).then((con) => {
//     // console.log(con.connections)dfads
//     console.log('DB connection successful')
// }).catch(error => console.log(error));

// Starting the server on port 4001
const port = process.env.PORT || 4001
app.listen(port, () => {
    console.log(`App running on port ${port} ..`)
})
