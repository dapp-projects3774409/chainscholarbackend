const mongoose = require('mongoose');

const questionSchema = new mongoose.Schema({
    communityName: {
        type: String,
        required: [true, 'Enter the name of the CommunityName']
    },
    description: {
        type: String,
        required: [true, 'Enter some detail of the question']
    },
    email: {
        type: String,
        required: [true, 'Enter the name of the author']
    },
    answers: {
        type: Array,
        default: [],
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
});

// // Middleware to generate and increment the questionId
// questionSchema.pre('save', function (next) {
//     const doc = this;
//     if (!doc.questionId) {
//         mongoose.model('Question').findOne({}, {}, { sort: { 'questionId': -1 } }, function (err, lastQuestion) {
//             if (err) {
//                 return next(err);
//             }
//             doc.questionId = lastQuestion ? lastQuestion.questionId + 1 : 1;
//             next();
//         });
//     } else {
//         next();
//     }
// });

const Question = mongoose.model('Question', questionSchema);

module.exports = Question;
