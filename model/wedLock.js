const mongoose = require('mongoose');

// Define the schema for the Marriage Certificate
const marriageCertificateSchema = new mongoose.Schema({
    husbandName: {
        type: String,
        required: [true, 'Husband name is required'],
    },
    husbandAddress: {
        type: String,
        required: [true, 'Husband address is required'],
    },
    husbandDOB: {
        type: Date,
        required: [true, 'Husband date of birth is required'],
    },
    husbandCID: {
        type: String,
        required: [true, 'Husband CID is required'],
    },
    wifeName: {
        type: String,
        required: [true, 'Wife name is required'],
    },
    wifeAddress: {
        type: String,
        required: [true, 'Wife address is required'],
    },
    wifeDOB: {
        type: Date,
        required: [true, 'Wife date of birth is required'],
    },
    wifeCID: {
        type: String,
        required: [true, 'Wife CID is required'],
    },
    status: {
        type: String,
        enum: ['accepted', 'rejected', 'pending'],
        default: 'pending', // Default status is 'pending'
        required: true,
    },
    approvalAuthority: {
        type: String,
        required: [true, 'Approval authority wallet address is required'],
    },
    createdAt: {
        type: Date,
        default: Date.now,
    },
});

// Create the MarriageCertificate model
const MarriageCertificate = mongoose.model('MarriageCertificate', marriageCertificateSchema);

module.exports = MarriageCertificate;
