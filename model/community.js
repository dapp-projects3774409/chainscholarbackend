const mongoose = require('mongoose');

const communitySchema = new mongoose.Schema({
    communityName: {
        type: String,
        required: [true, 'Enter the name of the certificate']
    },
    email: {
        type: String,
        required: [true, 'Please provide the email ID'],
    },
    description: {
        type: String,
        required: [true, 'Enter some detail of the community']
    },
    photo: {
        type: String,
        default: 'default.jpg',
    },
    members: {
        type: Array,
        default: [],
    }
})

const Community = mongoose.model("Community", communitySchema)
module.exports = Community;