import express from "express";

const router = express.Router();

const userController = require("../controllers/auth");
const { userAuth } = require("../middleware/auth")

router.post("/register", userController.register);
router.post("/login", userController.login);
router.post("/verify", userController.verifyOTP);
router.post("/joinCommunity", userController.updateUser);



router.get("/:message", userController.showMessage);
router.get("/getuser/:id", userController.getUser)
router.get("/getUserDetails/:id", userController.getUserDetails)

router.put("/updateUserRPoints/", userController.updateUserRPoints)
router.put("/incrementUserRPoints/", userController.incrementUserRPoints)


router.post("/logout", userAuth, userController.logout)

// export default router;
//export the router so thatit can be used in other parts of your application
module.exports = router;
