const express = require("express");
const router = express.Router();
const QuestionController = require("../controllers/question");
const { userAuth } = require("../middleware/auth");

router.post("/createQuestion", QuestionController.createQuestion);
router.post("/updateQuestionAnswers", QuestionController.updateQuestionAnswers);
router.put("/updateAnswerInQuestion/:questionId", QuestionController.updateAnswerInQuestion);


router.get("/getAllQuestionsOfCommunity/:id", QuestionController.getAllQuestionsOfCommunity);
router.get("/getAllAnswersOfQuestion/:id", QuestionController.getAllAnswersOfQuestion);
// router.get("/getAllQuestionsOfUser/:id", QuestionController.getAllQuestionsOfUser);
router.get("/getAllAnswersOfUsers/:id", QuestionController.getAllAnswersOfUsers);

// router.delete("/delete/:id", certificateController.deleteCertificate);

module.exports = router;
