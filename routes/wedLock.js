const express = require("express");
const router = express.Router();
const MarriageCertificateController = require("../controllers/wedLock");

// Define the routes for the Marriage Certificate controller

// Create a new marriage certificate
router.post("/create", MarriageCertificateController.createMarriageCertificate);

// Update the status of a marriage certificate (accept/reject)
router.put("/updateStatus/:id", MarriageCertificateController.updateMarriageCertificateStatus);

// Get a marriage certificate by its ID
router.get("/getById/:id", MarriageCertificateController.getMarriageCertificateById);

// Get all marriage certificates
router.get("/getAllCertificates", MarriageCertificateController.getAllMarriageCertificates);

// Get all certificates approved by a specific authority (by wallet address)
router.get("/getByApprovalAuthority/:id", MarriageCertificateController.getAllCertificatesByApprovalAuthority);

// Get all certificates by status (accepted, rejected, pending)
router.get("/getByStatus/:status", MarriageCertificateController.getCertificatesByStatus);

// Get all certificates by status (WalletAddress)
router.get("/getByWalletAddress/:walletAddress", MarriageCertificateController.getCertificatesByWalletAddress);


module.exports = router;
