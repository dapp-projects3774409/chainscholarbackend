const express = require("express");
const router = express.Router();
const communityController = require("../controllers/community");
const { userAuth } = require("../middleware/auth");

router.post("/create", communityController.createCommunity);
router.get("/get/:id", communityController.getCommunity);
router.get("/getCommunity/:id", communityController.getCommunityWithEmail);
router.get('/searchCommunity/:id', communityController.searchCommunity)
router.get('/getAllCommunity', communityController.getAllCommunity)
router.post('/updateCommunityMember', communityController.updateCommunityMember)

router.get("/getUserCommunity/:id", communityController.getUserCommunity);
router.get("/getCommunityWithUserEmail/:id", communityController.getCommunityWithUserEmail);

// router.delete("/delete/:id", certificateController.deleteCertificate);

module.exports = router;
