const User = require("../model/user");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { generateAuthOTP } = require("../utility/otp");
const UserOtpVerification = require("../model/userOtpVerification");

export const showMessage = (req, res) => {
  res.status(200).send(req.params.message);
};

export const register = async (req, res) => {
  const { accountType, name, email, photo, password } = req.body;
  if (!name) return res.status(400).send("Name is required");

  //password length validation for debugging
  if (!password || password.length < 6)
    return res.status(400).send("Password should be a minimum of 6 characters");

  try {
    const userExist = await User.findOne({ email });
    if (userExist) return res.status(400).json({ error: "Email is taken" });

    const otp = await generateAuthOTP({ email });
    if (!otp) {
      return res.status(400).json({ status: "Failed", message: "OTP not generated" })
    }

    // Create a new user instance
    const newUser = await User.create({ accountType, name, email, photo, password });
    if (!newUser) {
      return res.status(400).json({ status: "Failed", message: "User not created" })
    }

    res.status(201).json({ status: "success", data: newUser });
  } catch (error) {
    console.error(error);
    res.status(400).send({
      status: "failed",
      message: error.message
    })
  }
};

export const login = async (req, res) => {
  const { email, password } = req.body;

  try {
    const user = await User.findByCredentials(email, password);
    if (user.verified) {
      console.log(user)

      const AuthToken = await user.generateAuthToken();

      res.cookie("session_id", AuthToken)

      res.status(200).json({ message: user, status: "Success" })
    } else {
      res.status(400).json({ message: "Invalid Credential", status: "Failed" })
    }
  }
  catch (e) {
    res.status(400).send(e)
  }
}

export const verifyOTP = async (req, res) => {
  try {
    let { email, otp } = req.body;

    if (!email || !otp) {
      throw Error("Empty otp details are not allowed.")
    } else {
      const userOTPVerificationRecords = await UserOtpVerification.find({ email })
      console.log("userOTPVer", userOTPVerificationRecords)

      if (userOTPVerificationRecords.length <= 0) {
        throw new Error("Account record doesn't exists or has been verified already. Please signup or login.");
      } else {
        const { expiresAt } = userOTPVerificationRecords[0];
        const hashedOTP = userOTPVerificationRecords[0].otp;
        console.log(Date.now())

        // Convert expiresAt to a timestamp
        const expiresAtTimestamp = Date.parse(expiresAt);

        console.log(expiresAtTimestamp)

        if (expiresAtTimestamp < Date.parse(Date.now())) {
          UserOtpVerification.deleteMany({ email });
          throw new Error("OTP has expired. Please request again.");
        } else {
          const validOTP = await bcrypt.compare(otp, hashedOTP);
          if (!validOTP) {
            throw new Error("Invalid code passed. check your inbox.")
          } else {
            await User.updateOne({ email }, { verified: true });
            await UserOtpVerification.deleteMany({ email });

            res.json({
              status: "Verified",
              message: "User email verified successfully."
            });
          }
        }
      }
    }
  } catch (e) {
    res.status(400).send({
      status: "Failed",
      message: e.message
    })
  }
}

// export const getUser = async (req, res) => {
//   try {
//     const user = await User.findOne({ where: { email: req.params.id } });
//     return res.json({ status: "success", message: user.accountType });
//   } catch (err) {
//     console.error(err);
//     return res.status(500).send("Server error");
//   }
// };


export const getUser = async (req, res) => {
  const { id: email } = req.params;
  if (!email) {
    return res.status(400).send("Email is required");
  }

  try {
    // Find all questions with the given email
    const community = await User.find({ email });

    console.log(community)

    // Return the count of questions found
    res.status(200).json({
      status: "success",
      data: {
        accountType: community[0].accountType // Number of questions found
      }
    });
  } catch (error) {
    console.error(error);
    res.status(400).send({
      status: "failed",
      message: error.message
    });
  }
};



export const getUserDetails = async (req, res) => {
  const { id: email } = req.params;
  if (!email) {
    return res.status(400).send("Email is required");
  }

  try {
    // Find all questions with the given email
    const community = await User.find({ email });

    console.log(community)

    // Return the count of questions found
    res.status(200).json({
      status: "success",
      data: {
        User: community // Number of questions found
      }
    });
  } catch (error) {
    console.error(error);
    res.status(400).send({
      status: "failed",
      message: error.message
    });
  }
};

export const updateUserRPoints = async (req, res) => {
  try {
    const { email, Rpoints } = req.body;

    if (!email) {
      return res.status(400).send({
        status: "failed",
        message: "Email is required"
      });
    }

    if (Rpoints === undefined || isNaN(Rpoints)) {
      return res.status(400).send({
        status: "failed",
        message: "Valid newRPoints is required"
      });
    }

    const updatedUser = await User.findOneAndUpdate(
      { email }, // Find the user by email
      { Rpoints: Rpoints }, // Update the rPoints field
      { new: true } // Return the updated document
    );

    if (!updatedUser) {
      return res.status(404).send({
        status: "failed",
        message: "User not found"
      });
    }

    res.status(200).json({
      status: "success",
      data: updatedUser,
      message: "RPoints updated successfully"
    });
  } catch (error) {
    console.error(error);
    res.status(500).send({
      status: "failed",
      message: "An error occurred while updating RPoints"
    });
  }
};

export const incrementUserRPoints = async (req, res) => {
  try {
    const { email } = req.body;

    if (!email) {
      return res.status(400).send({
        status: "failed",
        message: "Email is required"
      });
    }

    // Use $inc operator to increment Rpoints by 1 atomically
    const updatedUser = await User.findOneAndUpdate(
      { email },
      { $inc: { Rpoints: 1 } },
      { new: true }
    );

    if (!updatedUser) {
      return res.status(404).send({
        status: "failed",
        message: "User not found"
      });
    }

    res.status(200).json({
      status: "success",
      data: updatedUser,
      message: "RPoints incremented successfully"
    });
  } catch (error) {
    console.error(error);
    res.status(500).send({
      status: "failed",
      message: "An error occurred while updating RPoints"
    });
  }
};


export const logout = async (req, res) => {
  try {
    req.user.tokens = req.user.tokens.filter((token) => {
      return token.token !== req.token
    })
    await req.user.save()

    res.status(200).json({ message: "Logged Out successfully", status: "Success" })
  } catch (e) {
    res.status(400).send(e)
  }
}

//joining new community
export const updateUser = async (req, res) => {
  try {
    let { email, communityName } = req.body;

    // Check if the new email is provided
    if (!email) {
      return res.status(400).json({ status: "Failed", message: "New email is required" });
    }

    // Find the user by ID
    const user = await User.findById(email);

    // Check if the user exists
    if (!user) {
      return res.status(404).json({ status: "Failed", message: "User not found" });
    }

    // Update the user's email
    user.communityList.push(communityName)

    // Save the updated user
    await user.save();

    // Respond with success message
    return res.status(200).json({ status: "Success", message: "User email updated successfully" });
  } catch (error) {
    console.error(error);
    return res.status(500).json({ status: "Failed", message: "Internal server error" });
  }
};
