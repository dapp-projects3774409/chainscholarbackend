const MarriageCertificate = require('../model/wedLock');

// 1. Create a Marriage Certificate
export const createMarriageCertificate = async (req, res) => {
    const {
        husbandName,
        husbandAddress,
        husbandDOB,
        husbandCID,
        wifeName,
        wifeAddress,
        wifeDOB,
        wifeCID,
        approvalAuthority
    } = req.body;

    // Validation
    if (!husbandName || !wifeName || !approvalAuthority) {
        return res.status(400).send("Husband name, wife name, and approval authority wallet address are required");
    }

    try {
        // Create a new marriage certificate
        const newCertificate = await MarriageCertificate.create({
            husbandName,
            husbandAddress,
            husbandDOB,
            husbandCID,
            wifeName,
            wifeAddress,
            wifeDOB,
            wifeCID,
            approvalAuthority
        });

        res.status(201).json({ status: "success", data: newCertificate });
    } catch (error) {
        console.error(error);
        res.status(400).send({
            status: "failed",
            message: error.message
        });
    }
};

// 2. Update Marriage Certificate Status (accept/reject)
export const updateMarriageCertificateStatus = async (req, res) => {
    const { id: certificateId } = req.params;
    const { status } = req.body;

    if (!certificateId || !status) {
        return res.status(400).send("Certificate ID and status are required");
    }

    if (!['accepted', 'rejected'].includes(status)) {
        return res.status(400).send("Invalid status. Status must be 'accepted' or 'rejected'");
    }

    try {
        // Update the status of the marriage certificate
        const updatedCertificate = await MarriageCertificate.findByIdAndUpdate(
            certificateId,
            { status },
            { new: true } // Return the updated document
        );

        if (!updatedCertificate) {
            return res.status(404).json({ error: "Marriage certificate not found" });
        }

        res.status(200).json({ status: "success", data: updatedCertificate });
    } catch (error) {
        console.error(error);
        res.status(400).send({
            status: "failed",
            message: error.message
        });
    }
};

// 3. Get Marriage Certificate by ID
export const getMarriageCertificateById = async (req, res) => {
    const { id: certificateId } = req.params;

    if (!certificateId) {
        return res.status(400).send("Certificate ID is required");
    }

    try {
        const certificate = await MarriageCertificate.findById(certificateId);
        if (!certificate) {
            return res.status(404).json({ error: "Marriage certificate not found" });
        }

        res.status(200).json({ status: "success", data: certificate });
    } catch (error) {
        console.error(error);
        res.status(400).send({
            status: "failed",
            message: error.message
        });
    }
};

// 4. Get All Marriage Certificates
export const getAllMarriageCertificates = async (req, res) => {
    try {
        const certificates = await MarriageCertificate.find();
        if (!certificates || certificates.length === 0) {
            return res.status(404).json({ error: "No marriage certificates found" });
        }

        res.status(200).json({ status: "success", data: certificates });
    } catch (error) {
        console.error(error);
        res.status(400).send({
            status: "failed",
            message: error.message
        });
    }
};

// 5. Get All Certificates by Approval Authority
export const getAllCertificatesByApprovalAuthority = async (req, res) => {
    const { id: walletAddress } = req.params;

    if (!walletAddress) {
        return res.status(400).send("Approval authority wallet address is required");
    }

    try {
        const certificates = await MarriageCertificate.find({ approvalAuthority: walletAddress });
        if (!certificates || certificates.length === 0) {
            return res.status(404).json({ error: "No certificates found for the specified authority" });
        }

        res.status(200).json({ status: "success", data: certificates });
    } catch (error) {
        console.error(error);
        res.status(400).send({
            status: "failed",
            message: error.message
        });
    }
};

// 6. Get All Marriage Certificates by Status
export const getCertificatesByStatus = async (req, res) => {
    const { status } = req.params;

    // Validate status
    if (!status || !['accepted', 'rejected', 'pending'].includes(status)) {
        return res.status(400).send("Invalid status. Status must be 'accepted', 'rejected', or 'pending'");
    }

    try {
        // Find certificates by status
        const certificates = await MarriageCertificate.find({ status });

        if (!certificates || certificates.length === 0) {
            return res.status(404).json({ error: `No marriage certificates found with status: ${status}` });
        }

        res.status(200).json({ status: "success", data: certificates });
    } catch (error) {
        console.error(error);
        res.status(400).send({
            status: "failed",
            message: error.message
        });
    }
};


export const getCertificatesByWalletAddress = async (req, res) => {
    const { walletAddress } = req.params;

    if (!walletAddress) {
        return res.status(400).json({ error: 'Wallet address is required' });
    }

    try {
        const certificates = await MarriageCertificate.find({
            $or: [{ husbandAddress: walletAddress }, { wifeAddress: walletAddress }],
        });

        if (!certificates.length) {
            return res.status(404).json({ error: 'No certificates found for the provided wallet address' });
        }

        res.status(200).json(certificates);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Internal server error' });
    }
};
