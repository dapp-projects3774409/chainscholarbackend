const Community = require('../model/community')
const User = require('../model/user')

export const createCommunity = async (req, res) => {
    const { communityName, email, description } = req.body;
    if (!communityName) return res.status(400).send("Certificate name is required");

    try {
        const community = await Community.findOne({ communityName });
        if (community) return res.status(400).json({ error: "Community already registered" });

        const newCommunity = await Community.create({ communityName, email, description });
        if (!newCommunity) return res.status(400).json({ error: "Community not created" });

        res.status(201).json({ status: "success", data: newCommunity });
    } catch (error) {
        console.error(error);
        res.status(400).send({
            status: "failed",
            message: error.message
        })
    }
}

export const getCommunity = async (req, res) => {
    const { id: communityName } = req.params;

    try {
        const community = await Community.findOne({ communityName: communityName });
        return res.status(200).json({ status: "success", data: community });
    }
    catch (error) {
        console.error(error);
        res.status(400).send({
            status: "failed",
            message: error.message
        })
    }
}

export const getCommunityWithEmail = async (req, res) => {
    const { id: email } = req.params;

    try {
        const community = await Community.findOne({ email: email });
        return res.status(200).json({ status: "success", data: community });
    }
    catch (error) {
        console.error(error);
        res.status(400).send({
            status: "failed",
            message: error.message
        })
    }
}

export const getAllCommunity = async (req, res) => {
    try {
        const communities = await Community.find();
        return res.status(200).json({ status: "success", data: communities });
    } catch (error) {
        console.error(error);
        res.status(500).send({
            status: "failed",
            message: "Internal server error"
        })
    }
}


// Define your searchCommunity function
export const searchCommunity = async (req, res) => {
    try {

        const { id: payload } = req.params;

        let search = await Community.find({ communityName: { $regex: new RegExp("^" + payload + ".*", "i") } }).exec();
        // limit search result to ten
        // search = search.slice(0, 10); // Uncomment this line if you want to limit the search result to ten

        res.send({ payload: search });
    } catch (error) {
        console.error("Error searching community:", error);
        res.status(500).send({ error: "Internal Server Error" });
    }
};




export const updateCommunityMember = async (req, res) => {
    const { email, communityName } = req.body;
    if (!email || !communityName) {
        return res.status(400).send("Email and communityName are required");
    }

    try {
        const community = await Community.findOne({ communityName: communityName });

        if (!community) {
            return res.status(404).json({ error: "Community not found" });
        }

        // Check if the email is already a member of the community
        const isMember = community.members.includes(email);
        if (isMember) {
            return res.status(200).json({ status: "success", message: "Email is already a member of the community" });
        }

        // Push the new answers object into the answers array of the question
        community.members.push(email);

        // Save the updated question
        await community.save();

        res.status(200).json({ status: "success", message: community });
    } catch (error) {
        console.error(error);
        res.status(400).send({
            status: "failed",
            message: error.message
        });
    }
}



export const updateCertificate = async (req, res) => {
    try {
        const certificate = await Certificate.findOne({ where: { id: req.params.id } });
        certificate.certificateName = req.body.certificateName;
        certificate.courseName = req.body.courseName;
        certificate.coursePeriod = req.body.coursePeriod;
        certificate.courseDetails = req.body.courseDetails;
        certificate.gpa = req.body.gpa;
        await certificate.save();
        return res.status(200).json({ status: "success", data: certificate });

    }
    catch (error) {
        console.error(error);
        res.status(400).send({
            status: "failed",
            message: error.message
        })
    }
}

export const getUserCommunity = async (req, res) => {
    const { id: email } = req.params;
    if (!email) {
        return res.status(400).send("Email is required");
    }

    try {
        // Find all questions with the given email
        const community = await Community.find({ email });

        console.log(community)

        // Return the count of questions found
        res.status(200).json({
            status: "success",
            data: {
                CommunityList: community // Number of questions found
            }
        });
    } catch (error) {
        console.error(error);
        res.status(400).send({
            status: "failed",
            message: error.message
        });
    }
};

export const getCommunityWithUserEmail = async (req, res) => {
    const { id: email } = req.params;

    try {
        const communities = await Community.find(); // Fetch all communities

        const communitiesWithEmail = [];

        // Iterate over each community and check if the email exists in members
        for (const community of communities) {
            console.log("Community Members:", community.members);
            console.log("Checking for email:", email);
            // Check if the email exists in the members array
            const hasEmail = community.members.includes(email);
            console.log(hasEmail)
            if (hasEmail) {
                communitiesWithEmail.push(community);
            }
        }

        return res.status(200).json({ status: "success", data: communitiesWithEmail });
    } catch (error) {
        console.error(error);
        res.status(400).send({
            status: "failed",
            message: error.message
        });
    }
};
