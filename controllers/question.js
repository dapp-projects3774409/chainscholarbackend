const Question = require('../model/question')
const User = require('../model/user')

export const createQuestion = async (req, res) => {
    const { communityName, description, email } = req.body;
    if (!communityName) return res.status(400).send("Community name is required");

    try {
        // const question = await Question.findOne({ communityName });
        // if (question) return res.status(400).json({ error: "Community already registered" });

        const newQuestion = await Question.create({ communityName, description, email });
        // if (!newQuestion) return res.status(400).json({ error: "Question not created" });

        res.status(201).json({ status: "success", data: newQuestion });
    } catch (error) {
        console.error(error);
        res.status(400).send({
            status: "failed",
            message: error.message
        })
    }
}


export const updateQuestionAnswers = async (req, res) => {
    const { questionId, answers } = req.body;
    if (!questionId || !answers) {
        return res.status(400).send("Question ID and answers are required");
    }

    try {
        const question = await Question.findById(questionId);
        if (!question) {
            return res.status(404).json({ error: "Question not found" });
        }

        // Push the new answers object into the answers array of the question
        question.answers.push(answers);

        // Save the updated question
        await question.save();

        res.status(200).json({ status: "success", data: question });
    } catch (error) {
        console.error(error);
        res.status(400).send({
            status: "failed",
            message: error.message
        });
    }
}

export const updateAnswerInQuestion = async (req, res) => {
    const { questionId } = req.params;
    const { answerId } = req.body;

    // if (!questionId || !answerId) {
    //     return res.status(400).send("Question ID and Answer ID are required");
    // }
    console.log(questionId, answerId)

    try {
        // Find the question and update the specific answer within the 'answers' array 
        const updatedQuestion = await Question.findOneAndUpdate(
            { _id: questionId }, // Find the question
            { $inc: { [`answers.${answerId}.voteCount`]: 1 } }, // Increment voteCount of the answer at index answerId
            { new: true } // Return the modified document
        );

        console.log(updatedQuestion)
        if (!updatedQuestion) {
            return res.status(404).json({ error: "Question or answer not found" });
        }

        res.status(200).json({ status: "success", data: updatedQuestion });

    } catch (error) {
        console.error(error);
        res.status(400).send({
            status: "failed",
            message: error.message,
            questionId,
            answerId
        });
    }
};

export const getAllQuestionsOfCommunity = async (req, res) => {
    const { id: communityName } = req.params;
    if (!communityName) {
        return res.status(400).send("Community name is required");
    }

    try {
        const questions = await Question.find({ communityName: communityName });
        if (!questions || questions.length === 0) {
            return res.status(404).json({ error: "No questions found for the specified community" });
        }

        // Store all questions in an array
        const allQuestions = questions.map(question => ({
            id: question._id,
            communityName: question.communityName,
            description: question.description,
            author: question.author,
            createdAt: question.createdAt,
            answers: question.answers
        }));

        res.status(200).json({ status: "success", data: allQuestions });
    } catch (error) {
        console.error(error);
        res.status(400).send({
            status: "failed",
            message: error.message
        });
    }
}
export const getAllAnswersOfQuestion = async (req, res) => {
    const { id: questionId } = req.params; // Change to id
    console.log(questionId);
    if (!questionId) {
        return res.status(400).send("Question ID is required");
    }

    try {
        const question = await Question.findById(questionId);
        if (!question) {
            return res.status(404).json({ error: "Question not found" });
        }

        // Store all answers in an array
        const allAnswers = question.answers.map(answer => ({
            description: answer.description,
            author: answer.author,
            voteCount: answer.voteCount,
            createdAt: answer.createdAt
        }));

        res.status(200).json({ status: "success", data: allAnswers });
    } catch (error) {
        console.error(error);
        res.status(400).send({
            status: "failed",
            message: error.message
        });
    }
}

export const getAllAnswersOfUsers = async (req, res) => {
    const { id: email } = req.params;

    if (!email) {
        return res.status(400).send("Email is required");
    }

    try {
        // Find all questions
        const questions = await Question.find();

        let totalAnswersByAuthor = 0;

        // Iterate through each question
        for (const question of questions) {
            // Iterate through the array of answers for the current question
            for (const answer of question.answers) {
                // Check if the answer's author matches the provided email
                if (answer.author === email) {
                    totalAnswersByAuthor++; // Increment the count if it does
                }
            }
        }

        // Calculate points based on answer count
        let points = Math.floor(totalAnswersByAuthor / 1);

        res.status(200).json({
            status: "success",
            data: {
                points: points // Include points in the response
            }
        });
    } catch (error) {
        console.error(error);
        res.status(400).send({
            status: "failed",
            message: error.message
        });
    }
};